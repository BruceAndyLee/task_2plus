package edu.phystech.demochat.service;

import edu.phystech.demochat.dataasset.dto.MessageDTO;
import edu.phystech.demochat.dataasset.pojo.ChatPOJO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChatService {

    private Map<String, ChatPOJO> chats;

    public ChatService() {
        this.chats = new HashMap<>();
    }

    public ResponseEntity addMessage(MessageDTO messageDTO, String chat){

        // Validate income information
        if(null == messageDTO.getName()
        || null == messageDTO.getText()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Incomplete income data");
        }

        // Get Chat for insert
        ChatPOJO chatPojo = getChatById(chat);
        chatPojo.addMessage(messageDTO);

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<String> getMessages(String chat){

        // Get Chat for read
        ChatPOJO chatPOJO = chats.get(chat);
        if(null == chatPOJO){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Chat not found");
        }

        return ResponseEntity.ok(chatPOJO.getMessages());
    }

    private ChatPOJO getChatById(String chat){

        ChatPOJO chatPOJO = chats.get(chat);
        if(null != chatPOJO){
            return chatPOJO;
        }

        ChatPOJO newChat = new ChatPOJO();
        chats.put(chat, newChat);
        return newChat;
    }
}
