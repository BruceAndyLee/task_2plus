package edu.phystech.demochat.dataasset.pojo;

import edu.phystech.demochat.dataasset.dto.MessageDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatPOJO {

    private List<MessageDTO> messages;

    public ChatPOJO() {
        this.messages = new ArrayList<>();
    }

    public void addMessage(MessageDTO messageDTO){
        messageDTO.setTime(new SimpleDateFormat("hh:mm").format(new Date()));
        messages.add(messageDTO);

    }

    public String getMessages(){
        StringBuilder result = new StringBuilder();
        for (MessageDTO x : messages){
            result.append(x.getTime())
                    .append(" ")
                    .append(x.getName())
                    .append(" >> ")
                    .append(x.getText())
                    .append("\n");
        }
        return result.toString();
    }

}
