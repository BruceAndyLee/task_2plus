/*package edu.phystech.demochat.dataasset.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.integration.IntegrationProperties;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@Repository
public class ChatRepository extends IntegrationProperties.JdbcDaoSupport {

    private DataSource dataSource;

    @Autowired
    public ChatRepository(DataSource dataSource){
        this.dataSource = dataSource;
    }

    @PostConstruct
    private void init() {
        setDataSource(dataSource);

        //this.getConnection().createStatement().execute();
        // SERIAL == autoincrement; sequence
        getJdbcTemplate().execute("drop table if exists messages");
        getJdbcTemplate().execute("create table messages (id SERIAL, sender sender_id INT, text VARCHAR(4000), receiver_id INT");
    }

    public void addMessage(int sender_id, int receiver_id, String text) throws SQLException {
        try (Connection conn = getConnection();
             Statement stmt = conn.createStatement()){

            int c = stmt.executeUpdate("insert into messages (sender_id, receiver_id, text) values (?,?,?)");

        }
    }


}*/