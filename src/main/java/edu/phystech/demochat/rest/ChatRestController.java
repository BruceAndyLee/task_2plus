package edu.phystech.demochat.rest;

import edu.phystech.demochat.dataasset.dto.MessageDTO;
import edu.phystech.demochat.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ChatRestController {

    @Autowired
    private ChatService chatService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addMessage(@RequestBody MessageDTO messageDTO, @RequestParam(defaultValue = "general") String chat){

        return chatService.addMessage(messageDTO, chat);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getMessages(@RequestParam(defaultValue = "general") String chat){

        return chatService.getMessages(chat);
    }

}
